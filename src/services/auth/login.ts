import { LoginType } from "@/types/login";

export const login = (data: LoginType) => {
  return new Promise(resolve => {
    setTimeout(() => {
      console.log("Logged in successfully.", data);
      resolve(data);
    }, 2000);
  });
};
