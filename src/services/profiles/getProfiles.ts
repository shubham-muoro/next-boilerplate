export const getProfilesQueryKey = () => ["profiles"];

export const getProfiles = () => {
  // api call using fetch (do not use axios)

  return new Promise(resolve => {
    setTimeout(() => {
      resolve([
        { id: 1, name: "ABC" },
        { id: 2, name: "DEF" },
      ]);
    }, 1500);
  });
};
