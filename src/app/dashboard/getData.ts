import { getProfiles } from "@/services/profiles";

const getData = async () => {
  const data = await getProfiles();
  return data;
};

export default getData;
