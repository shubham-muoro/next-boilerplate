import { FeatureCard, LoadMoreProfiles } from "@/components/dashboard";

import getData from "./getData";

const DashboardPage = async () => {
  const data = await getData();

  return (
    <div>
      Hello from Dashboard
      <FeatureCard />
      <pre>{JSON.stringify(data, null, 2)}</pre>
      <LoadMoreProfiles />
    </div>
  );
};

export default DashboardPage;
