import type { Meta, StoryObj } from "@storybook/react";
import { fn } from "@storybook/test";
import { Button } from "@/components/common";

const meta = {
  title: "Common Components/Button",
  component: Button,
  parameters: {
    layout: "centered",
  },
  tags: ["autodocs"],
  argTypes: {
    type: { control: "select", options: ["button", "submit"] },
    text: { control: "text", description: "Button Text" },
  },
  args: { onClick: fn() },
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    text: "Button",
  },
};

export const SubmitButton: Story = {
  args: {
    type: "submit",
    text: "Button",
  },
};
