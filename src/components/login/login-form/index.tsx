"use client";

import { zodResolver } from "@hookform/resolvers/zod";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import { FormProvider, useForm } from "react-hook-form";

import { Button, ControllerInput } from "@/components/common";
import { getDashboardRoute } from "@/lib/routes";
import { login } from "@/services/auth";
import { loginSchema, LoginType } from "@/types/login";

const LoginForm = () => {
  const router = useRouter();
  const methods = useForm<LoginType>({
    defaultValues: { email: "", password: "" },
    resolver: zodResolver(loginSchema),
  });

  const { mutateAsync } = useMutation({
    mutationFn: (values: LoginType) => login(values),
    onSuccess: () => {
      router.push(getDashboardRoute());
    },
  });

  const handleSubmit = async (values: LoginType) => {
    await mutateAsync(values);
  };

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(handleSubmit)}>
        <ControllerInput name="email" label="Email" type="text" />
        <ControllerInput name="password" label="Password" type="password" />
        <Button type="submit" text="Login" />
      </form>
    </FormProvider>
  );
};

export default LoginForm;
