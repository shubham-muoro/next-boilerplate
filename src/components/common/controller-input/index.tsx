import { useController } from "react-hook-form";

import Input, { InputProps } from "../input";

interface ControllerInputProps extends Pick<InputProps, "label" | "type"> {
  name: string;
}

const ControllerInput: React.FC<ControllerInputProps> = ({
  name,
  ...restProps
}) => {
  const { field } = useController({ name });

  return <Input {...restProps} {...field} />;
};

export default ControllerInput;
