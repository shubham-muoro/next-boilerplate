import { forwardRef } from "react";

export interface InputProps {
  label?: string;
  type?: "text" | "password";
  value: string;
  onChange: React.ChangeEventHandler<HTMLInputElement>;
}

const Input = forwardRef<HTMLInputElement, InputProps>(function Input(
  { type = "text", label, ...restProps },
  ref
) {
  return (
    <div>
      {label && <label>{label}</label>}
      <input ref={ref} type={type} {...restProps} />
    </div>
  );
});

export default Input;
