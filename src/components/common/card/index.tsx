import clsx from "clsx";

import styles from "./styles.module.scss";

interface CardProps {
  heading: string;
  body: string;
  extraClasses?: string;
}

const Card: React.FC<CardProps> = ({ heading, body, extraClasses }) => {
  const containerClasses = clsx(styles.cardContainer, extraClasses);

  return (
    <div className={containerClasses}>
      <div className={styles.cardHeading}>{heading}</div>
      <div className={styles.cardBody}>{body}</div>
    </div>
  );
};

export default Card;
