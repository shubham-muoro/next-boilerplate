"use client";

import { useQuery } from "@tanstack/react-query";

import { Button } from "@/components/common";
import { getProfiles, getProfilesQueryKey } from "@/services/profiles";

const LoadMoreProfiles = () => {
  const profilesQueryKey = getProfilesQueryKey();
  const { isLoading, data, refetch } = useQuery({
    queryKey: profilesQueryKey,
    queryFn: getProfiles,
    enabled: false,
  });

  const handleLoadMore = () => {
    refetch();
  };

  if (isLoading) return <div>Loading...</div>;

  return (
    <div>
      {data ? (
        <pre>{JSON.stringify(data, null, 2)}</pre>
      ) : (
        <Button text="Load" onClick={handleLoadMore} />
      )}
    </div>
  );
};

export default LoadMoreProfiles;
