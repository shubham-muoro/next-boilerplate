"use client";

import { Button, Card } from "@/components/common";

const FeatureCard = () => {
  const handleClick = () => {
    console.log("Button Clicked");
  };

  return (
    <div>
      <Card
        heading="Feature 1"
        body="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quos fugiat soluta nam? Voluptates qui deleniti, non autem distinctio cupiditate est quaerat tenetur tempore sunt reiciendis, magnam voluptatem accusantium facilis ex!"
      />

      <Button text="Start" onClick={handleClick} />
    </div>
  );
};

export default FeatureCard;
