export const getLoginRoute = () => {
  return "/login";
};

export const getDashboardRoute = () => {
  return "/dashboard";
};

type ProductRouteParams = {
  productId: string;
};
export const getProductRoute = ({ productId }: ProductRouteParams) => {
  return `/products/${productId}`;
};
